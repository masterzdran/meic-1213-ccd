'''
3 - Implemente analisador de sequencias admitindo que foram geradas por fonte
que e uma cadeia de Markov de 1.a ordem. Estime a entropia e implemente um simulador 
de fonte com memoria usando as frequencias de ocorrencia determinadas com base 
numa sequencia de simbolos (ficheiro). Analise varios tipos de ficheiro e os novos 
ficheiros obtidos com o simulador. Verifique a compressao obtida nos varios casos 
usando uma aplicacao de compressao (por exemplo WinZIP) e comente os resultados.
'''

# auxiliary functions

def check_debug(arg):
	return (arg=="True" or arg=="T" or arg=="true" or arg=="t")

def pretty_print_dict(dicto, before="", middle="", after=""):
	'''
	prints a dictionary in a fancy way
	'''
	for key, val in dicto.items():
		print ("%s %s %s %s %s\n"%(before, key, middle, val, after))

# parte 1 -> gerar a estatistica, sendo a fonte uma cadeia de Markov de 1a ordem

def get_statistics(file_path, debug):

	symbol_prob_dicto_dicto = dict()

	# markov de ordem = 1 -> 256 entradas com 256 entradas cada
	# para a combinacao do simbolo actual e a sua dependencia de 
	# mais um simbolo...

	with open(file_path,"rb") as input_data:
		
		# este codigo nao contempla a hipotese do input de 
		# simbolos so ter um simbolo.

		current_symbol = input_data.read(1)
		next_symbol = input_data.read(1)
		sequence_size = 2

		while next_symbol:

			if current_symbol not in symbol_prob_dicto_dicto:
				symbol_prob_dicto_dicto[current_symbol] = dict()

			if next_symbol not in symbol_prob_dicto_dicto[current_symbol]:
				symbol_prob_dicto_dicto[current_symbol][next_symbol] = 1

			else:
				symbol_prob_dicto_dicto[current_symbol][next_symbol] += 1

			current_symbol = next_symbol
			next_symbol = input_data.read(1)
			sequence_size += 1

	return (symbol_prob_dicto_dicto, sequence_size)

# aid method
def from_int_dicto_to_float_dicto(input_dicto_dicto, debug):
	'''
	passar do dicionario de dicionarios com o numero de 
	ocorrencias dos simbolos para um dicionario de dicionarios 
	com as probabilidades de cada simbolo 
	'''

	entropy = 0

	for current_symbol, dicto in input_dicto_dicto.items():
		
		total_occurences = 0
		#tmp_float_dicto = dict()

		for occurrence in dicto.values():
			total_occurences += occurrence

		for next_symbol in dicto.keys():
			dicto[next_symbol] = float( dicto[next_symbol] ) / float( total_occurences )
			if debug:
				print "P(%s|%s)=%f"%(next_symbol, current_symbol, dicto[next_symbol])

	entropy_dicto = calculate_entropy_dicto(input_dicto_dicto)
	if debug:
		pretty_print_dict(entropy_dicto, "H( p(", ") ) = ")

	return input_dicto_dicto

def calculate_entropy_dicto(probability_dicto_dicto):
	'''
	somatorio de todos os res(xi) tais que:  res(xi) = p(xi+1|xi) * ( log base 2 of (1 / (p(xi+1|xi))) )
	'''

	import math

	entropy_value = 0.0
	entropy_dicto = dict()

	for current_symbol in probability_dicto_dicto.keys():

		for dicto in probability_dicto_dicto.values():
			
			if current_symbol in dicto:
				
				probability = dicto[current_symbol]

				if probability > 0:
					entropy_value += ( probability * math.log( ( 1 / probability), 2) )
		

		entropy_dicto[current_symbol] = entropy_value
		entropy_value = 0.0

	return entropy_dicto

# auxiliary method
def generate_simbol_array_code_dicto(symbol_dicto_dicto):
	import sys
	import array

	tiny_diference = sys.float_info.min # float minimal value
	symbol_range = dict()
	internal_dicto = dict()

	accomulated_prob = 0.0

	for symbol, dicto in symbol_dicto_dicto.items():

		for next_symbol, prob in dicto.items():
			
			if prob>0.0: # current symbol -> *(next symbol, (min range, max range))
				internal_dicto[next_symbol] = (accomulated_prob, (accomulated_prob + prob - tiny_diference) )
			
			accomulated_prob += prob

		symbol_range[symbol] = internal_dicto
		internal_dicto = dict()
		accomulated_prob = 0.0

	return symbol_range

# parte 2 -> usar um simulador de uma fonte que seja uma cadeia de Markov de 1a 	
# ordem com a estatistica da (parte 1)

def generate_file(file_path, nr_symbols, symbol_dicto_dicto, debug):

	import random as rand

	# symbol dict() -> dict() (key, value=(min_range, max_range) ) para codificacao aritmetica
	symbol_dicto_nextSymbol_range_dicto = generate_simbol_array_code_dicto(symbol_dicto_dicto)

	if debug:
		print " symbol -> next symbol [min range, max range["
		for s in symbol_dicto_nextSymbol_range_dicto.keys():
			for next in symbol_dicto_nextSymbol_range_dicto[s].keys():
				min, max = symbol_dicto_nextSymbol_range_dicto[s][next]
				print "%s -> %s [%f, %f[" % (s, next, min, max)

	# get the starting symbol
	current_symbol = rand.choice( symbol_dicto_nextSymbol_range_dicto.keys() )

	with open(file_path,"wb") as file_output:
		
		writen_symbols = 1
		file_output.write(current_symbol)
		next_symbol_prob = rand.random()

		while writen_symbols < nr_symbols:
			
			writen_symbols, current_symbol = write_current_symbol(
				writen_symbols, 
				file_output, 
				symbol_dicto_nextSymbol_range_dicto[current_symbol], 
				current_symbol,
				next_symbol_prob, 
				debug
			)

			next_symbol_prob = rand.random()
			
		
		file_output.flush()
		

def write_current_symbol(
	writen_symbols, 
	file_output, 
	current_symbol_dicto, 
	current_symbol,
	next_prob, 
	debug
	):

	for next_symbol, range in current_symbol_dicto.items():
				
		min_range, max_range = range
		
		if (next_prob >= min_range) and (next_prob <= max_range):
			
			if debug:
				print "going from %s to %s with probability %f<%f<%f" % (current_symbol, next_symbol, min_range, next_prob, max_range)

			file_output.write(next_symbol)
			return ( (writen_symbols + 1), next_symbol )

	# if the code reaches here, there was a point were we didn't find a way out of
	# the current node. what should I do? end the symbol generator?
	raise Exception("current node doesn't have an exit!")

# --------
# - main -
# --------
if __name__ == '__main__':
	import sys

	if len(sys.argv)>1:

		debug = False # default = false

		if len(sys.argv)>2: 
			debug = check_debug(sys.argv[3])

		symbol_dicto, sequence_size = get_statistics(
			sys.argv[1],
			debug
		)
		
		if len(sys.argv)>2:
			sequence_size = int(sys.argv[2])

		if debug:
			print "sequence size = %s" % sequence_size

		generate_file(
			sys.argv[1]+".markovgen",
			sequence_size,
			from_int_dicto_to_float_dicto(symbol_dicto, debug),
			debug
		)

		exit()
		
	print "usage:\n<this script> <data input file> <[optional, default=size input file]output file size> <[optional, default=False] debug (values=T or True)>"

