"""
4 - Implementar simulador de fonte com memoria (pelo menos para cadeia de Markov
de 1a ordem) baseado no metodo de C. Shannon:
To construct "second-order approximation, i. e., first-order Markov chain," for example, 
one opens a book at random and selects a letter at random on the page. This letter is 
recorded. The book is then opened to another page and one reads until this letter is 
encountered. The succeeding letter is then recorded. Turning to another page this second 
letter is searched for and the succeeding letter recorded, etc. A similar process was 
used for "high-order approximations".
"""

from markovtools.markovchain import MarkovChain

def read_file(file_path, chain, model_order, total_symbols, debug):
	# special case when K = 0 -> read file only once and add all chars to symbol occurrence
	# with K = 0 all symbols are independent

	# else K > 0
	readed_words = 0

	with open(file_path, "rb") as input_text:

		ch = input_text.read(1)
		k_gram_str = ch
		internal_order = 1

		while total_symbols>readed_words:

			if internal_order>=model_order:
				old_pos = input_text.tell()
				next_ch = input_text.read(1)

				if not next_ch: # if end reached then "treat the input text as a circular input..."
					input_text.seek(0) # go to the beginning of the file
					next_ch = input_text.read(1)

				input_text.seek( old_pos ) # re-position file pointer to current char

				chain.addTransition( # add new_word (markov node) and the next char
					k_gram_str, 
					next_ch	
				) 

				if debug:
					print("debug: added transition from node {} to {}").format(k_gram_str, next_ch)
								
				readed_words += 1
				k_gram_str = k_gram_str[1:]
				internal_order = len(k_gram_str)

			ch = input_text.read(1)
			if not ch: # if end reached then "treat the input text as a circular input..."
				input_text.seek(0) # go to the beginning of the file
				ch = input_text.read(1)

			k_gram_str += ch
			internal_order += 1


def write_file(file_path, chain, output_file_size, debug):
	
	import random as rand

	current_symbol = rand.choice(chain.get_all_node_keys()) # choose at random a first symbol
	written_symbols = len(current_symbol)
	if written_symbols>output_file_size:
		return

	with open(file_path,"wb") as output_file:

		output_file.write(current_symbol)
		if debug:
			print("debug: output = {}").format(current_symbol) 

		next_ch = chain.next(current_symbol)

		while written_symbols<output_file_size:
			output_file.write(next_ch)

			if debug:
				print("debug: output = {}").format(next_ch) 

			current_symbol = current_symbol[1:] + next_ch
			next_ch = chain.next(current_symbol)
			written_symbols += 1


# --------
# - main -
# --------
if __name__=="__main__":
	import sys
	import os

	if len(sys.argv)>2:

		debug = False # default false
		if len(sys.argv)>4: 
			debug = check_debug(sys.argv[4])

		file_path = sys.argv[1]

		model_order = int(sys.argv[2])

		if debug:
			print("k={} for input file '{}'").format(model_order, file_path)

		chain = MarkovChain()

		# total words = count of individual symbols in input text
		total_symbols = os.path.getsize(file_path) # file size in bytes = accepted text range 256 symbols = 1 byte
	
		# create markov symbol chain
		read_file(file_path, chain, model_order, total_symbols, debug)
		
		if debug:
			chain.print_me()

		# generate file
		output_file_size = total_symbols
		if len(sys.argv)>3: 
			output_file_size = int(sys.argv[3])

		write_file(file_path+".gen", chain, output_file_size, debug)

		exit()

	print("usage:\n<this script> <data input file> <Markov model order>  <[optional, default=input file size] file size> <[optional, default=False] debug (values=T or True)>")
