import random as rand
import sys

class MarkovNode:
	"""
	Represents a markov node with a key string
	and a value that are this node neighbour nodes
	"""

	#fields are added when the obj is constructed...
	#__key
	#__char_neighbours # string -> probability
	#__total_char_neighbours

	def __init__(self, k=""): # construtor
		self.__key=k
		self.__char_neighbours=dict()
		self.__total_char_neighbours = 0

	def add_neighbour(self, neighbour):
		'''
		receiving neighbour as a string...
		'''
		if not neighbour: 
			return

		try:
			if self.__char_neighbours_probs is not None: # invalidate symbol probabilities due to adition of new symbol
				self.__char_neighbours_probs = None
		except:
			self.__char_neighbours_probs = None

		if neighbour in self.__char_neighbours.keys():
			self.__char_neighbours[neighbour] += 1
		else:
			self.__char_neighbours[neighbour] = 1

		self.__total_char_neighbours += 1

	def __generate_symbol_probs(self):
		
		tiny = sys.float_info.min # float minimal value
		acumutator = 0.0
		for neighbour, prob in self.__char_neighbours.items():
			temp_calc_prob = float(prob) / float(self.__total_char_neighbours)
			self.__char_neighbours_probs[neighbour] = (acumutator, (acumutator + temp_calc_prob - tiny) )
			acumutator += temp_calc_prob

	def get_random_neighbour(self):
		try: # pythonian "easier to ask for forgiveness than permission" (EAFP)
			if self.__char_neighbours_probs is None:
				self.__char_neighbours_probs = dict()
				self.__generate_symbol_probs()

		except:
			self.__char_neighbours_probs = dict()
			self.__generate_symbol_probs()

		next_probability = rand.random() # get probability [0,1[
		
		for neighbour, prob in self.__char_neighbours_probs.items():
			min_range, max_range = prob
			if next_probability>=min_range and next_probability<=max_range:
				return neighbour 


		return None
		
	def print_me(self, before=""): # pretty print
		print "{}total neighbours={}".format(before, self.__total_char_neighbours)
		print "{}char\tprob".format(before)
		for key, val in self.__char_neighbours.items():
			print ("{}{}{}{}".format(before, key, "\t", val))
		
	def __str__(self): # to string
		return "i'm node {}".format(self.__key)