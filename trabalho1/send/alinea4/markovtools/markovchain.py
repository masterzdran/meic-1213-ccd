from markovnode import MarkovNode

class MarkovChain:

	#__total_node_transitions
	#__nodes_k_probs_nodes # -> node key -> (probability, node)

	def __init__(self): # construtor
		self.__total_node_transitions = 0;
		self.__nodes_k_probs_nodes = dict();

	def addTransition(self, origin_node, next_symbol):
		'''
		add a transition from state origin_node to state next_symbol
		'''

		if not origin_node: 
			return

		if origin_node in self.__nodes_k_probs_nodes.keys():
			# node key already contained...
			prob, node = self.__nodes_k_probs_nodes[origin_node]
			node.add_neighbour(next_symbol)
			self.__nodes_k_probs_nodes[origin_node] = (prob+1, node)

		else:
			# this origin node is new around here...
			new_node = MarkovNode(origin_node)
			new_node.add_neighbour(next_symbol)
			self.__nodes_k_probs_nodes[origin_node] = (1, new_node)
		
		self.__total_node_transitions += 1		

	def get_all_node_keys(self):
		return self.__nodes_k_probs_nodes.keys();

	def next(self, origin_node):
		'''
		pick a transition leaving state origin_node uniformly at random, and return the resulting state
		or by other words:  return a random neighbor of state origin_node
		'''
		_, node = self.__nodes_k_probs_nodes[origin_node]
		return node.get_random_neighbour()


	def print_me(self): # pretty print
		print self.__str__
		print "total node transitions = {}".format(self.__total_node_transitions)		
		for node, pair in self.__nodes_k_probs_nodes.items():
			prob, int_markov_node = pair
			print "\nnode {} has occurred {} times and contains neighbors:".format(node, prob)
			int_markov_node.print_me("\t")


	def __str__(self):
		'''
		return a string representation of this Markov chain
		'''
		return "total nodes = {}".format(__total_node_transitions)