#!/usr/bin/perl

use strict;
use warnings;


=pod
src: http://en.wikipedia.org/wiki/Golomb_coding

Simple algorithm
--------------------------------------------------------------------------------
Note below that this is the Rice-Golomb encoding, where the remainder code uses 
simple truncated binary encoding, also named "Rice coding" (other varying-length
 binary encodings, like arithmetic or Huffman encodings, are possible for the 
 remainder codes, if the statistic distribution of remainder codes is not flat, 
 and notably when not all possible remainders after the division are used). 
 In this algorithm, if the M parameter is a power of 2, it becomes equivalent to
  the simpler Rice encoding.
1- Fix the parameter M to an integer value.
2- For N, the number to be encoded, find
    2.1- quotient = q = int[N/M]
    2.2- remainder = r = N modulo M
3- Generate Codeword
    3.1- The Code format : <Quotient Code><Remainder Code>, where
    3.2- Quotient Code (in unary coding)
        3.2.1- Write a q-length string of 1 bits
        3.2.2- Write a 0 bit
    3.3- Remainder Code (in truncated binary encoding)
        3.3.1- If M is power of 2, code remainder as binary format. So b = log2(M)) bits are needed. (Rice code)
        3.3.2- If M is not a power of 2, set 
            3.3.2.1- If r < 2^b - M :  code r as plain binary using b-1 bits.
            3.3.2.2- If r >= 2^b - M code the number r + 2^b - M  in plain binary representation using b bits.
=cut
my $M = 8;
my $N = 256;


for (my $i = 0;$i<$N;$i++)
{
    print encode($i);
} 



sub encode
{
    my ($symbol) = @_;
    my $quotient = int $symbol/$M;
    my $remainder = $symbol % $M;
    
    my $q = quotientCode($quotient);
    my $r = remainderCode($remainder);
    
    return sprintf("%10.10s| %30.30s | %5.5s| %s%s \n",$symbol,$q,$r,$q,$r);

}

sub isPowerOfTwo {
     my ($number) = @_;
     
     my $power = 2;
     
     while ($power <= $number)
     {
        if ($power == $number)
        {
            return 1;
        }
        $power= $power << 1;
     }
     return 0;
}

sub logBase2 
{
    my ($value) = @_;
    
    return int log($value)/log(2);
}

sub remainderCode 
{
   my ($remainder) = @_;
   my $str="";
   if(isPowerOfTwo($M))
   {
        #Rice Code
        my $b = logBase2($M);
        $str.=sprintf("%b",$remainder);
    
   }
   return $str;
}

sub quotientCode 
{
    my ($qLen) = @_;
    
    my $str ="";
    while($qLen--)
    {
        $str.="1";
    }
    $str.="0";
    return $str;   
}
