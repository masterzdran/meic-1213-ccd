#!C:/Python27
# -*- coding: latin-1 -*-

def check_debug(arg):
	return (arg=="True" or arg=="T" or arg=="true" or arg=="t")

def check_decode(arg):
	return not (arg=="Decode" or arg=="D" or arg=="decode" or arg=="d")

def generate_range_map(input_file, debug):
	'''
	input file format:
	<total_simbol symbols>
	<s1><nr times s1 apeared>
	<s2><nr times s2 apeared>
	...
	returns dictionary with symbol -> (min range, max range)
	'''

	import sys
	import math

	# iter(lambda: file_obj.read(4), ""): ...
	symbol_range_min_max = dict()
    
	with open(input_file,"rb") as f_in:

		total_simbols = float(f_in.readline())
		tiny_diference = sys.float_info.min # float minimal value
		acomulated_probl = 0
		line = f_in.readline()
		
		while line != "":
			if line[1:] != "":
				key = line[:1]
				val = int(line[1:])

				# usar os simbolos empilhados por prob de zero a um
				symbol_prob = float(val / total_simbols) 
				symbol_range_min_max[key] = (acomulated_probl,  acomulated_probl + symbol_prob - tiny_diference)
				
				#debug
				if debug: 
					print("{} will have range ({}, {}) ".format(key, acomulated_probl, acomulated_probl + symbol_prob - tiny_diference))
				
				acomulated_probl += symbol_prob

			line = f_in.readline()
		
		#debug
		if debug: 
			print("prob = {} (should be 1)".format(acomulated_probl))
		
		return symbol_range_min_max

def get_inputs( first_arg, probability_dicto_file_path, file_size, debug, enc_or_not_dec):
	
	data = None
	if enc_or_not_dec: # if enconding... get data
		with open(first_arg, "rb") as input_file:
			data = input_file.read()
	else:
		 # if decoding... get value
		 data = float(first_arg)

	probability_dicto = generate_range_map( probability_dicto_file_path, debug)


	return (probability_dicto, data, int(file_size))

def get_ranges_for_symbol_with_probability(prob, probability_dicto):
	for symbol, range in probability_dicto.items():
		min_range, max_range = range
		if (prob >= min_range) and (prob <= max_range):
			return (symbol, min_range, max_range)

def arithmetic_encode(data, probability_dicto, file_size):
	'''
	encondes the data using the probability_dicto probabilities
	in the form of: symbol -> ( min_range, max_range ) 
	'''
	
	bottom = 0.0
	top = 1.0

	#for ch in data:
	for i in range(file_size):
		ch = data[i]
		diference = top - bottom
		min_range, max_range = probability_dicto[ch]
		top = bottom + diference * max_range
		bottom = bottom + diference * min_range

	return (bottom, len(data))

def arithmetic_decode(value, probability_dicto, length):
	'''
	decodes the data using the probability_dicto probabilities
	in the form of: symbol -> ( min_range, max_range ) 
	'''

	result = []
	diference = 0.0
	for i in range(length):
		ch, min_range, max_range = get_ranges_for_symbol_with_probability(value, probability_dicto)
		result.append(ch)
		diference = max_range- min_range
		value = value - min_range
		value = value / diference

	return result

# --------
# - main -
# --------
if __name__ == '__main__':
	import sys

	if len(sys.argv)>4:

		debug = False # default = false

		if len(sys.argv)>5: 
			debug = check_debug(sys.argv[5])

		enc_or_not_dec = check_decode(sys.argv[4])

		probability_dicto, data, file_size = get_inputs(
			sys.argv[1],
			sys.argv[2],
			sys.argv[3],
			debug,
			enc_or_not_dec
		)

		if enc_or_not_dec: # encoding time!
			
			code, size = arithmetic_encode(data, probability_dicto, file_size)
			print("result of encoding = {} with length = {}".format(code, size))
		else:
			# decoding time
			result = arithmetic_decode(data, probability_dicto, file_size)
			print("result of decoding = {}".format(result))

		exit()
		
	print("usage:\n<this script> <data input (file path for encode or probability for decode)> <probability dictionary file path> <file length (32 bit int)> <[default=e] e-> encode, d->decode> <[optional, default=False] debug (values=T or True)>"
