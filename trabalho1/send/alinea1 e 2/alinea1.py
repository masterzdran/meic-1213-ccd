'''
1 - Assumindo que um ficheiro e uma sequencia de simbolos (bytes) produzida por uma
fonte discreta cujo alfabeto tem 256 simbolos, desenvolva uma aplicacao que recebendo 
o nome do ficheiro determine a frequencia de ocorrencia de cada simbolo persistindo 
essa informacao num formato a estabelecer (para usar no simulador) e estime a entropia 
da fonte assumindo que esta nao tem memoria. Use a aplicacao desenvolvida para analisar 
varios tipos de ficheiros (texto, codigo, imagens, compactados, audio, video, etc.).
'''

#----------------------
# utils
# Math.random() % 256 # max 256 simbolos
# log (base 2) (x) = ( log (base a) (x) ) / ( log (base a) (2) ) 
# (entropia) H(X) = somatorio de todos os (xi) tal que: p(xi) * ( log (base 2) (1 / (p(xi))) ) 

#----------------------
# aid functions

def check_debug(arg):
	return (arg=="True" or arg=="T" or arg=="true" or arg=="t")

def pretty_print_dict(dicto):
	'''
	prints a dictionary in a fancy way
	'''
	for key, val in dicto.items():
		print ("%s %s\n"%(key,val))

def count_symbols(input_data, symbol_map):
	'''
	counts the symbols in the input data, returning the total of symbols readed
	'''
	total_symb = 0

	byt = input_data.read(1)
	while byt:
		total_symb+=1

		val = symbol_map.get(byt)

		if val is None:
			symbol_map[byt] = 1 # create entry and set to 1 the entry count
		else:
			symbol_map[byt] = val+1 # increment the entry count

		byt = input_data.read(1)

	return total_symb

def save_statistics(total, stats, output_file, show_stats):
	'''
	saves probability statistics in a file
	'''
	output_statistics = open(output_file,"wb")
	output_statistics.write("%d\n"%total) # write the total of simbols
	output_statistics.flush()
	for key, val in stats.items():
		output_statistics.write("%s"%key) # write the symbol
		output_statistics.write("%s\n"%val) # write the number of times that symbol appears
 
	if show_stats: # show statistics
		print ("total symbols = %d")%(total)
		# pretty_print_dict(stats)

def calculate_entropy(probability_map):
	'''
	somatorio de todos os res(xi) tais que:  res(xi) = p(xi) * ( log base 2 of (1 / (p(xi))) )
	'''
	import math

	entropy_value = 0.0

	for symbol, probability in probability_map.items():
		if probability > 0:
			entropy_value += ( probability * math.log( (1 / probability), 2) )

	return entropy_value

def analisys(input_file, show_stats=False):
	'''
	analysis input file
	'''
	
	symbol_map = dict() # dictonary to hold symbol (key) -> nr times that symbol apeared (value)
	with open(input_file,"rb") as f_in: # com  o ficheiro na variavel f_in, vamos contar cada simbolo
		total_symb = count_symbols(f_in, symbol_map)

	prob_map = dict() # dictionary to hold symbol (key) -> symbol probability (value)

	# calculate prob. per symbol: p(xi)
	for sy, val in symbol_map.items():
		prob_map[sy] = float(val) / float(total_symb)

	# calculate entropy
	entropy = calculate_entropy(prob_map)

	# save statistics in file
	save_statistics(total_symb, symbol_map, input_file+".stats", show_stats)
	
	if show_stats:
		print "entropy = %f" % entropy
	
	print "finished analysis"

#----------------------
# program init

if __name__ == '__main__':

	import sys

	if len(sys.argv)>2:
		analisys(
			sys.argv[1], 
			check_debug( sys.argv[2] ) 
		)
		exit()

	if len(sys.argv)>1:
		analisys(sys.argv[1])
		exit()

	print "usage:\n<this script> <data input file> <[optional] show statistics (values=T or True)>"
