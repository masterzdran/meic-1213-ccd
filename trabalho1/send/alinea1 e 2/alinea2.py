'''
2 - Implemente simulador de fonte discreta sem memoria para gerar sequencias de simbolos
(ficheiros), dadas a dimensao da sequencia e a informacao da frequencia de ocorrencia no 
formato estabelecido.
'''

# auxiliary functions

def check_debug(arg):
	return (arg=="True" or arg=="T" or arg=="true" or arg=="t")

# generate ( symbol | min_range | max_range ) map

def generate_range_map(input_file, debug=False):
	'''
	input file format:
	<total_simbol symbols>
	<s1><nr times s1 apeared>
	<s2><nr times s2 apeared>
	...
	returns dictionary with symbol -> (min range, max range)
	'''

	import sys
	import math

	# iter(lambda: file_obj.read(4), ""): ...
	symbol_range_min_max = dict()
    
	with open(input_file,"rb") as f_in:

		total_simbols = float(f_in.readline())
		tiny_diference = sys.float_info.min # float minimal value
		acomulated_probl = 0
		line = f_in.readline()
		
		while line != "":
			if line[1:] != "":
				key = line[:1]
				val = int(line[1:])

				# usar os simbolos empilhados por prob de zero a um
				# e depois com o random.rand() obter um valor entre 
				# zero e um e imprimir esse simbolo
				symbol_prob = float(val / total_simbols) 
				symbol_range_min_max[key] = (acomulated_probl,  acomulated_probl + symbol_prob - tiny_diference)
				
				#debug
				if debug: 
					print "%s will have range (%f, %f) "%(key, acomulated_probl, acomulated_probl + symbol_prob - tiny_diference) 
				
				acomulated_probl += symbol_prob

			line = f_in.readline()
		
		#debug
		if debug: 
			print "prob = %f (should be 1)"%(acomulated_probl)
		
		return symbol_range_min_max



if __name__ == '__main__':
	import sys
	import random as rand

	if len(sys.argv)>2:
		symbol_range_min_max = generate_range_map(
			sys.argv[1] 
		)

		debug = False
		if len(sys.argv)>3: 
			debug = check_debug(sys.argv[3])

		# now that we have a map in the form:
		#	symbol, min_prob_range, max_prob_range

		sequence_length = int(sys.argv[2])

		with open(sys.argv[1]+".generated","wb") as output_file:

			# all we need to do is generate a sequence with sequence_length simbols
			# were each symbol is generated from a random function

			nr_symbols_writen = 0 
			# using a var to control the writen symbols aids in the situation that the random
			# generated symbol is between [max_range, 1.0) and the writing symbol mechanism
			# doesn't work 

			while sequence_length>nr_symbols_writen:
				target = rand.random() # get a random float in interval [0.0, 1.0)
				# see which symbol corresponds to this float
				# by checking were the target float fits in the (min_range, max_range) interval
				for key, range_pair in symbol_range_min_max.items():
					min_range, max_range = range_pair # unpack range min and max values
					if target>=min_range and target<=max_range:
						output_file.write(key)
						nr_symbols_writen+=1

			output_file.flush()

		exit()
		
	print "usage:\n<this script> <data input file> <sequence size (32 bit integer)> <[optional] debug (values=T or True)>"