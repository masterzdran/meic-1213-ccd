﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CompressionAnalysis
{
    internal class Program
    {
        #region string pahts

        private static string _basePath = Directory.GetCurrentDirectory();

        private static string _sevenZaPath = _basePath + @"/tools/7za920/7za.exe";

        private static string _inputFilesPath = _basePath + @"/testFiles/";

        private static string _outputFile = _basePath + @"/tmp/test.7z";

        #endregion

        private const string PreparedOutputString = "{0},{1};{2};{3};{4};{5}\n";

        /// <summary>
        /// algorithm, compression, [flags], initial file size, compressed file size, time spent in milis, memory spent in bytes, input file
        /// </summary>
        /// <returns>
        /// a file with name /outputs/input file name_algorithm_DETAILS.txt with the statistic for that file
        /// </returns>
        /// <param name="args"></param>
        private static void PrintInfo(object[] args)
        {
            var str = args[7].ToString();

            var outputFileName = str.Substring(
                str.LastIndexOf('/')+1,
                ( str.LastIndexOf('.') - str.LastIndexOf('/') - 1 )
            );

            File.AppendAllText(
                string.Format("{0}/outputs/{1}_{2}_DETAILS.txt", _basePath, outputFileName, args[0] ),
                string.Format(
                        PreparedOutputString,
                        args[1], args[2], args[3], args[4], args[5], args[6]
                    )
                );
        }
        
        private static void LaunchProcess(
            string inputFile,
            string algorithm = "PPMd",
            int compression = 0,
            int memory = 24,
            int modelOrder = 6,
            int chunkSize = 1
            )
        {
            try{
                string algorithmFlags = algorithm.Equals("PPMd") ? // if ...
                                            string.Format(
                                                "a -t7z -m{0}=PPMd:mem={1}:o={2}",
                                                compression,
                                                memory,
                                                modelOrder
                                                  )
                                            : // else ...
                                            string.Format(
                                                "a -t7z -m{0}=LZMA2:c={1}:d={2}", 
                                                compression, 
                                                chunkSize, 
                                                memory
                                            );

                string cmdArgs =
                    algorithmFlags + " "
                    + _outputFile + " "
                    + inputFile;

                var proc = new Process();

                proc.StartInfo.FileName = _sevenZaPath;
                proc.StartInfo.Arguments = cmdArgs;

                #region testing purposes

                //var proc = new Process();
                //proc.StartInfo.FileName = "cmd.exe";
                //proc.StartInfo.Arguments = "/C exit";

                #endregion

                proc.StartInfo.CreateNoWindow = true;
                proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

                proc.EnableRaisingEvents = true;

                long procTime = 0;

                proc.Exited += (sender, evntArgs) =>
                                   {
                                       try
                                       {
                                           procTime = proc.ExitTime.Subtract(proc.StartTime).Ticks;
                                       }catch
                                       {
                                           // do nothing
                                       }

                                   };

                long memoryConsumed = 0;

                proc.Start();

                try 
	            {
                    while (!proc.HasExited) // ugly way to get process data...
                    {     
		                memoryConsumed = proc.PeakVirtualMemorySize64;
                        Thread.Sleep(250);
                    }
                }
                catch
                {
                    // do nothing here because this exception is due to the process termination 
                    // and not giving time to the methods gather info about the processor time
                    // and memory it has consumed.
                }

                var inputFileInfoLength = new FileInfo(inputFile).Length;

                long outputFileInfoLength = 0;
                if (File.Exists(_outputFile))
                {
                    outputFileInfoLength = new FileInfo(_outputFile).Length;
                    File.Delete(_outputFile);
                }
                else
                {
                    //output file was not created...
                    return;
                }

                PrintInfo(
                    new object[]
                        {
                            algorithm,
                            compression,
                            string.Format("{0},{1}", 
                                memory, 
                                algorithm.Equals("PPMd") ? modelOrder : chunkSize
                            ),
                            inputFileInfoLength,
                            outputFileInfoLength,
                            procTime,
                            memoryConsumed,
                            inputFile
                        }
                    );

                proc.Close();
            }
            catch(Exception e)
            {
                Console.WriteLine(
                    "EXCEPTION at {0} :: {1}", 
                    DateTime.Now.ToString(CultureInfo.CurrentCulture),
                    e.Message
                );
            }
        }

        static void Main(string[] args)
        {
            string[] algorithms = new[] { "PPMd", "LZMA2" };

            //int[] compressionArray = new[] { 0, 1, 3, 5, 7, 9 }; // level of compression
            int[] compressionArray = new[] { 0, 5, 9 }; // level of compression -> 9 maximum

            int[] memoryArray = new[]{ 2, 29, 1}; // min, max, pace -> min and max inclusive
            // memory used (PPMd) and dictionary size (LZMA) -> min = 2^2 bytes AND max = 2^29 bytes 

            int[] modelOrderArray = new[] { 2, 32, 2 }; // min, max, pace -> min and max inclusive

            int[] chunkSizeArray = new[] { 1, 4, 1 }; // min, max, pace -> min and max inclusive

            var start = DateTime.Now;
            Console.WriteLine("Start = {0}",start.ToString(CultureInfo.CurrentCulture));

            foreach (var al in algorithms)
            {
                foreach (var input in Directory.GetFileSystemEntries(_inputFilesPath))
                {
                    foreach (var comp in compressionArray)
                    {
                        for (int mem = memoryArray[0]; mem <= memoryArray[1]; mem += memoryArray[2])
                        {
                            if (al.Equals("PPMd"))
                            {
                                for (int model = modelOrderArray[0]; model <= modelOrderArray[1]; model += modelOrderArray[2])
                                { // so usa estes parâmetros se for o PPMd
                                    LaunchProcess(
                                        input,
                                        al,
                                        comp,
                                        mem,
                                        model,
                                        0 // ignored in PPMd
                                    );
                                }
                            }
                            else
                            {
                                for (int chunk = chunkSizeArray[0]; chunk <= chunkSizeArray[1]; chunk += chunkSizeArray[2])
                                {
                                    LaunchProcess(
                                            input,
                                            al,
                                            comp,
                                            mem,
                                            0,// ignored in LZMA2
                                            chunk
                                    );
                                }
                            }
                        }
                    }
                }
            }
            
            var end = DateTime.Now;
            Console.WriteLine("End = {0}", end.ToString(CultureInfo.CurrentCulture));
            Console.WriteLine("Duration = {0}", end.Subtract(start));

            Console.ReadLine();
        }
    }
}
