import pickle
import os
import random

#########################################
## maps/tuples, zip function and range ##
#########################################
d = dict( zip( 'abc', range(3) ) )

for idx, val in d.items():
	print idx," -> " ,val

##################################################
## pickling -> transform any object in a string ##
##################################################
xpto = pickle.dumps("this is a string?") # string to pickle
data = pickle.loads(xpto) # pickle to string
print data

################################
## pipes -> call OS functions ##
################################
print "\n~~running command 'dir' \n"
cmd = 'dir' # in linux = 'ls -l'
pointer = os.popen(cmd)

res = pointer.read()
print " read = ",res

res = pointer.close()
print " close = ",res

###########################
##  Classes (FINALLY!!!) ##
###########################
class Point(object):
	"""Represents a point in 2D Space"""

blank = Point()
print "blank =",blank

blank.x = 5
blank.y = -1

print "x =",blank.x
print "y =",blank.y
try:
	print "z =",blank.z # doesn't exist...
except:
	print "variable z doesn't exist in object blank of type Point"

print '(%g, %g)' % (blank.x, blank.y)

def print_point(p):
	print '(%g, %g)' % (p.x, p.y)

try:
	blank2 = object()
	blank2.x = 12 # this throws an exception
	blank2.y = 3
	print_point(blank) # point
	print_point(blank2) # fake point...
except:
	print "unable to fake point object"
	
###########################
##  Classes ...revisited ##
############################
class Rectangle:
	""" represents a rectangle object """
	
	x = 0 # no need to declare this...
	y = 0 # no need to declare this...
	
	def __init__(self,x=1,y=1):
		self.x=x;
		self.y=y;
		
	def print_me(self):
		print 'x=%g, y=%g' % (self.x, self.y)
		
	def __str__(self):
		return '(%g, %g)' % (self.x, self.y)
	
#	def __add__(self,rectangle): # overrides operator '+' -> version 1
#		self.x += rectangle.x
#		self.y += rectangle.y
#		return self
	
	def __add__(self,other): # overrides operator '+' -> version 2
		if isinstance(other,Rectangle): # if 'other' is a Rectangle...
			res = Rectangle()
			res.x = self.x + other.x
			res.y = self.x + other.y
			return res
		#else...
		return Rectangle()
	
	def __radd__(self,other): # overrides operator '+' -> RIGHT side adition = Object + Rectangle (in __add__ we had = Rectangle + Object)
		return self.__add__(other)

#end of Rectangle class

rec = Rectangle() # rectangle 1
rec.x = 5
rec.y = 4
rec.print_me()

rec2 = Rectangle() # rectangle 2
print rec2

rec3 = rec + rec2 # rectangle 3 -> sum of last 2 rectangles
print rec3

print 'rectangle 3 is rectangle?', (rec3 is rec) # in version 1 of the '+' implementation this comparation is TRUE
print 'rectangle 3 is rectangle 2?', (rec3 is rec2)

##################
##  Inheritance ##
##################

class Card:
	""" represents a standard playing card. """
	
	suit_names = ['Paus', 'Ouros', 'Copas', 'Espadas']
	rank_names = [None,'As','2','3','4','5','6','7','8','9','10','Valete','Dama','Rei']
	
	def __init__(self, suit = 0, rank = 2): # default card = 2 of Clubs (2 de Paus)
		self.suit = suit
		self.rank = rank
		
	def __str__(self):
		return '%s de %s' % (Card.rank_names[self.rank], 
							Card.suit_names[self.suit])
		
	def __cmp__(self, other):
		t1 = self.rank, self.suit # create tuple with -> self
		t2 = other.rank, other.suit 
		return cmp(t1,t2) # return comparison of tuples
		
c1 = Card()
print "carta basica = ", c1

class Deck:
	""" represents a deck of cards. """
	
	def __init__(self): # default constructor builds a deck of cards...
		self.cards = []
		for suit in range(4):
			for rank in range(1,14):
				card = Card(suit,rank)
				self.cards.append(card)
				
	def __str__(self):
		res = []
		for card in self.cards:
			res.append(str(card))
		
		return '\n'.join(res) # return str(res) -> provoques a return of the list in just one line. '\n'.join(...) appends a new line char to all elements
	
	def pop_card(self):
		return self.cards.pop() # pops last card from the deck (bottom card)
	
	def append_card(self, card):
		self.cards.append(card) # adds a card to beggining of the deck
		
	def shuffle(self):
		random.shuffle(self.cards) # shuffles the deck... pretty much that...
	
d1 = Deck()
#print "\nbaralho 1:"
#print d1 # prints the deck -> loads of lines so i've commented this

class Hand(Deck): # Hand extends Deck
	"""represents a hand of playing cards"""
	def __init__(self, label=''): # default constructor overrides parent class default constructor
		self.cards = []
		self.label = label
		
	def move_cards(self, hand, num_cards): # can move cards between Deck and Hand object type interchangebly
		for i in range(num_cards):
			hand.add_card(self.pop_card())

	
h1 = Hand()
print "\nDeck 1 is Hand 1?", (d1 is h1) 
print "Deck 1 == Hand 1?", (d1 == h1) 